var express = require("express");
var app = express();

app.route('*').all(function(req, res) {
    res.redirect(301, 'http://www.guesty.com/blog' + req.originalUrl);
});

var port = process.env.PORT || 5000;
app.listen(port, function() {
    console.log("Listening on " + port);
});